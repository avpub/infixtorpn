<?php

function infix_to_rpn(string $expr): string
{
    $tokens = str_split($expr);

    $output = [];
    $opstack = [];
    foreach ($tokens as $token) {
        if (is_operand($token)) {
            $output[] = $token;
            continue;
        }

        if ('(' === $token) {
            array_push($opstack, $token);
        } elseif (')' === $token) {
            while('(' !== $head = end($opstack)) {
                $output[] = $head;
                array_pop($opstack);
            }
            array_pop($opstack);
        } else {
            while($head = end($opstack)) {
                if ('(' === $head) {
                    break;
                }

                if (weight($head)[0] > weight($token)[0]) {
                    $output[] = $head;
                }

                if (weight($head)[0] === weight($token)[0] && 'lft' === weight($head)[1]) {
                    $output[] = $head;
                }

                if (weight($head)[0] < weight($token)[0]) {
                    break;
                }

                array_pop($opstack);
            }

            array_push($opstack, $token);
        }
    }

    while (!empty($opstack)) {
        $output[] = array_pop($opstack);
    }

    return implode($output);
}

function weight(string $op): array
{
    $ops = [
        '^' => [4, 'rgt'],
        '*' => [3, 'lft'],
        '/' => [3, 'lft'],
        '+' => [2, 'lft'],
        '-' => [2, 'lft'],
    ];

    if (!array_key_exists($op, $ops)) {
        throw new \InvalidArgumentException();
    }

    return $ops[$op];
}

function is_operand(string $operand): bool
{
    return ctype_alpha($operand);
}

$exps = readline();
$exprs = [];
while ($exps > 0) {
    $exprs[] = readline();
    $exps--;
}
foreach ($exprs as $expr) {
    echo infix_to_rpn($expr).PHP_EOL;
}
